﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContaCorrente_EF.Model;

namespace ContaCorrente_EF.Negocio
{
    class ContaNegocio
    {
        public static Conta Depositar(Conta conta, float valor)
        {
            conta.Saldo = conta.Saldo + valor;
            return conta;
        }

        public static Conta Sacar(Conta conta, float valor)
        {
            if (conta.Saldo >= valor)
            {
                conta.Saldo = conta.Saldo - valor;
                return conta;
            }
            return null;
        }
    }
}

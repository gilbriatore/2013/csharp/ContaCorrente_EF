﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContaCorrente_EF.Model
{
    class Conta
    {
        public int Id { set; get; }
        public virtual Cliente Cliente { set; get; }
        public int Agencia { set; get; }
        public int NumConta { set; get; }
        public float Saldo { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContaCorrente_EF.Model
{
    class Cliente
    {
        public int Id { set; get; }
        public string Cpf { set; get; }
        public string Nome { set; get; }
    }
}

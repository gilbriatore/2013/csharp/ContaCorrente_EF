﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContaCorrente_EF.Model;
using ContaCorrente_EF.DAO;
using ContaCorrente_EF.Negocio;
namespace ContaCorrente_EF
{
    class Program
    {
        static void Main(string[] args)
        {
            int opc;
            do
            {                
                Console.WriteLine("\n\nCONTA CORRENTE\n\n");
                Console.WriteLine("1 - Cadastrar clientes");
                Console.WriteLine("2 - Cadastrar contas");
                Console.WriteLine("3 - Depositar");
                Console.WriteLine("4 - Sacar");
                Console.WriteLine("5 - Saldo");
                Console.WriteLine("6 - Sair");
                Console.Write("\n\nOpção: ");
                opc = int.Parse(Console.ReadLine());
                switch (opc)
                {
                    case 1:
                        IncluirClientes();
                        break;
                    case 2:
                        CadastrarContas();
                        break;
                    case 3:
                        Depositar();
                        break;
                    case 4:
                        Sacar();
                        break;
                    case 5:
                        MostrarSaldo();
                        break;
                }
            } while (opc != 6);
        }

        private static void IncluirClientes()
        {
            Console.Clear();
            Cliente cliente = new Cliente();
            Console.Write("CPF: ");
            cliente.Cpf = Console.ReadLine();
            if (ClienteNegocio.ValidarCpf(cliente))
            {
                if (ClienteDAO.Search(cliente) == null)
                {
                    Console.Write("Nome: ");
                    cliente.Nome = Console.ReadLine();
                    if (ClienteDAO.Insert(cliente))
                    {
                        Console.WriteLine("Inclusão bem sucedida.");
                    }
                }
                else
                {
                    Console.WriteLine("Cliente já cadastrado.");
                }
            }
            else
            {
                Console.WriteLine("CPF inválido.");
            }
            Console.ReadKey();
        }

        private static void CadastrarContas()
        {
            Console.Clear();
            Cliente cliente = new Cliente();
            Console.Write("CPF: ");
            cliente.Cpf = Console.ReadLine();
            cliente = ClienteDAO.Search(cliente);
            if (cliente != null)
            {
                Conta conta = new Conta();
                conta.Cliente = cliente;
                Console.Write("Agência: ");
                conta.Agencia = int.Parse(Console.ReadLine());
                Console.Write("Conta: ");
                conta.NumConta = int.Parse(Console.ReadLine());
                if (ContaDAO.Search(conta) == null)
                {
                    Console.Write("Saldo: ");
                    conta.Saldo = float.Parse(Console.ReadLine());
                    if (ContaDAO.Insert(conta))
                    {
                        Console.WriteLine("Inclusão bem sucedida.");
                    }
                }
                else
                {
                    Console.WriteLine("Conta já cadastrada.");
                }
            }
            else
            {
                Console.WriteLine("Cliente não cadastrado.");
            }
            Console.ReadKey();
        }

        private static void Depositar()
        {
            Console.Clear();
            Conta conta = new Conta();
            Console.Write("Agência: ");
            conta.Agencia = int.Parse(Console.ReadLine());
            Console.Write("Conta: ");
            conta.NumConta = int.Parse(Console.ReadLine());
            conta = ContaDAO.Search(conta);
            if (conta != null)
            {
                Console.Write("Valor: ");
                float valor = float.Parse(Console.ReadLine());
                conta = ContaNegocio.Depositar(conta, valor);
                if (ContaDAO.Update(conta))
                {
                    Console.WriteLine("Depósito realizado.");
                }
                else
                {
                    Console.WriteLine("Depósito não realizado.");
                }
            }
            else
            {
                Console.WriteLine("Conta não cadastrada.");
            }
            Console.ReadKey();
        }

        private static void Sacar()
        {
            Console.Clear();
            Conta conta = new Conta();
            Console.Write("Agência: ");
            conta.Agencia = int.Parse(Console.ReadLine());
            Console.Write("Conta: ");
            conta.NumConta = int.Parse(Console.ReadLine());
            conta = ContaDAO.Search(conta);
            if (conta != null)
            {
                Console.Write("Valor: ");
                float valor = float.Parse(Console.ReadLine());
                conta = ContaNegocio.Sacar(conta, valor);
                if(conta != null)
                {
                    if (ContaDAO.Update(conta))
                    {
                        Console.WriteLine("Saque realizado.");
                    }
                    else
                    {
                        Console.WriteLine("Saque não realizado.");
                    }
                }
                else
                {
                    Console.WriteLine("Saldo insuficiente.");
                }
            }
            else
            {
                Console.WriteLine("Conta não cadastrada.");
            }
            Console.ReadKey();
        }

        private static void MostrarSaldo()
        {
            Console.Clear();
            Conta conta = new Conta();
            Console.Write("Agência: ");
            conta.Agencia = int.Parse(Console.ReadLine());
            Console.Write("Conta: ");
            conta.NumConta = int.Parse(Console.ReadLine());
            conta = ContaDAO.Search(conta);
            if (conta != null)
            {
                Console.WriteLine("ID: " + conta.Id);
                Console.WriteLine("Cliente: " + conta.Cliente.Nome);
                Console.WriteLine("Saldo: " + conta.Saldo.ToString("C2"));
            }
            else
            {
                Console.WriteLine("Conta não cadastrada.");
            }
            Console.ReadKey();
        }
    }
}

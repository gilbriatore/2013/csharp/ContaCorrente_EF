﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using ContaCorrente_EF.Model;
namespace ContaCorrente_EF.DAO
{
    class BancoEntities : DbContext
    {
        public DbSet<Cliente> Clientes {set; get;}
        public DbSet<Conta> Contas { set; get; }        
    }
}

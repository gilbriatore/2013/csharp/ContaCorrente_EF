﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContaCorrente_EF.Model;
namespace ContaCorrente_EF.DAO
{
    class ClienteDAO
    {
        public static bool Insert(Cliente cliente)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Clientes.Add(cliente);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Cliente Search(Cliente cliente)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                cliente = db.Clientes.FirstOrDefault(x => x.Cpf.Equals(cliente.Cpf));
                return cliente;
            }
            catch
            {
                return null;
            }
        }
    }
}

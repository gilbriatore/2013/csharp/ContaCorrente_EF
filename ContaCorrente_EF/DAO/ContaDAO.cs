﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContaCorrente_EF.Model;
using System.Data;

namespace ContaCorrente_EF.DAO
{
    class ContaDAO
    {
        public static bool Insert(Conta conta)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Contas.Add(conta);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Conta Search(Conta conta)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                conta = db.Contas.Include("Cliente").FirstOrDefault(x => x.Agencia == conta.Agencia && x.NumConta == conta.NumConta);
                return conta;
            }
            catch
            {
                return null;
            }
        }

        public static bool Update(Conta conta)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Entry(conta).State = EntityState. Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
